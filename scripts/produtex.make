################################################################################
# produtex.make
# $Id$
################################################################################
# Copyright 2011, 2012, 2013, 2014, 2015 Marco Emilio Poleggi
#
# This file is part of produtex.
#
# produtex is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# produtex is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with produtex.  If not, see <http://www.gnu.org/licenses/>.
################################################################################
# Variable intended to be used as parameter are UPPERCASE and should be reported
# by the 'help' target
################################################################################

.ONESHELL:
# .SHELLFLAGS = -e

################################################################################
# globals
################################################################################
srcdir		:= sources
sctsdir		:= sections
sctdir		:= $(srcdir)/$(sctsdir)
outdir		:= output
mytexdir	:= $(HOME)/docs/latex
JOB_OUTDIR	:=
DEF_MYJOB	:=
DEF_MYSELF	:=
DOC_TYPE	:=
# TEXINPUTS	?= $(HOME)/texmf
# TEXOUTPUT	?= $(srcdir)
anonym_tag	:= anonymous
ANONYMOUS	:= # undef = FALSE
NO_READER	:= # undef = FALSE

# executables -- lualatex doesn't produce some symbols like '°'???
# tex_engine	:= $(or $(shell which lualatex 2>&-),
tex_engine	:= $(or $(shell which pdflatex 2>&-),\
  $(error pdflatex not found: installation problem?))
bibtex_exe	:= bibtex
latex_exe	:= $(tex_engine) -file-line-error -output-directory=$(srcdir)
pdftxt_exe	:= pdftotext -nopgbrk -layout
cfiltr_exe 	:= $(or $(shell which cfilter 2>&-),\
  $(error cfilter not found: installation problem?))
prptex_exe 	:= $(or $(shell which preptex 2>&-),\
  $(error preptex not found: installation problem?))
pdfrdr_exe	:= $(or $(shell which $(PDF_READER) 2>&-),\
  $(or $(shell which mupdf 2>&-),\
    $(or $(shell which okular 2>&-),\
      $(or $(shell which acroread 2>&-),\
	  $(warning no PDF reader found)))))
pgrep_exe 	:= $(or $(shell which pgrep 2>&-),\
  $(warning pgrep not found; will not check if a PDF reader is running...))

PLANG		:= en
# babel language tags expansion, f.i, en -> english
llang		:= $(or \
  $(shell declare -A lngs; \
    lngs[fr]='french'; \
    lngs[en]='english'; \
    lngs[it]='italian'; \
    echo $${lngs[$(PLANG)]}\
  ), $(error cannot expand $(PLANG): unsupported language?)\
)

# input/output file names
custom_name	:=
custom_bname	:= $(notdir $(custom_name)).$(PLANG)
custom_dname	:= $(dir $(custom_name))
custom_src_tex	:= $(srcdir)/$(custom_dname)/$(custom_bname).tex
custom_out_pdf	:= $(custom_bname).pdf
# texput_pdf	:= $(srcdir)/$(if \
#   $(shell [[ $(tex_engine) =~ 'lualatex' ]] && echo true),texput,$(custom_bname)).pdf
doc_type	:= $(subst \ ,_,$(or $(DOC_TYPE), CV))
outf_prfx	:= $(subst \ ,_,$(or $(DEF_MYSELF), Author_Name))
bibtexdb	:= references
bibtexdb_src	:= $(mytexdir)/$(bibtexdb).bib

ifdef BIBTEX
  BIBTEX = $(or $(shell [ -r $(bibtexdb_src) ]),\
    $(error $(bibtexdb_src) not found/readable. Tip: set 'bibtexdb_src' variable))
endif

# TeX-style \defs are built via preptex
texdefs		:= \
	$(if $(DEF_MYSELF),-D MySelf='$(DEF_MYSELF)') \
	$(if $(DEF_MYJOB),-D MyJob='$(DEF_MYJOB)') \
	-D MyLang=$(llang)


################################################################################
# auxiliary functions
################################################################################
_minfo		= $(info [INFO] $(1))
_mwarn		= $(warning [WARN] $(1))
_merro		= $(error [ERROR] $(1))
_echoe		= echo -e
sinfo		= $(_echoe) '[INFO] '
swarn		= $(_echoe) '[WARN] '
serror		= $(_echoe) '[ERROR] '


################################################################################
# targets
################################################################################

# make an anonymous symlink to the input file, so that the output will
# be anonymous as well
.PHONY: _prepare_anon
_prepare_anon:
	@[ -r "$(srcdir)/$(input).tex" ] || {
	  $(serror) "$(srcdir)/$(input).tex: file not found/readable"
	  exit -1
	}
	cd $(srcdir) && ln -sf $(input).tex $(output).tex

_tojobdir:
	if [ $(JOB_OUTDIR) ]; then
	  mkdir -p $(JOB_OUTDIR) && \
	  cp $(source) $(JOB_OUTDIR) && \
	  $(sinfo) "$(source) copied to $(JOB_OUTDIR)"
	fi

# run latex with extra \defs
_dlatex:
	@$(sinfo) "texdefs: $(texdefs)"
	$(latex_exe) "`$(prptex_exe) $(texdefs) $(input).tex`"

# run latex + bibtex
.PHONY: _blatex _dlatex
_blatex:
	@$(MAKE) input=$(input) _dlatex || exit 1
	if [ "$(BIBTEX)" ]; then
	  $(bibtex_exe) $(srcdir)/$(input).aux && \
	  $(MAKE) input=$(input) _dlatex && \
	  $(MAKE) input=$(input) _dlatex
	fi

all:	custom

# however you try, you get artifacts when converting PDF to TXT, so, some
# extra filtering is done via a perl script...
#   <match>:<replacement>
txt: inputf := $(input).pdf
txt:
	@[ -r "$(inputf)" ] || {
	  $(serror) "$(inputf): not found or unreadable"
	  exit 1
	}
	$(pdftxt_exe) $(inputf) || exit 1
	$(cfiltr_exe) $(input).txt || exit 1


# run a PDF reader, unless one is already running, over input "pdffile"
_runreader: cmd_line := $(pdfrdr_exe) $(pdffile)
_runreader:
	@if [ "$(pgrep_exe)" ]; then
	  $(pgrep_exe) -lf "^$(cmd_line)$$" >/dev/null || $(cmd_line) &
	else
	  $(cmd_line) &
	fi

# create symlink named after the author
_symlinks:
	@cd $(outdir) || exit 1
	errs=0
	ftojdir=
	for ext in 'pdf' 'txt'; do
	  ln -sf $(target).$$ext $(symlnk).$$ext || {
	    errs=$$((errs+1))
	    continue
	  }
	  ftojdir+="$(symlnk).$$ext "
	done
	if [ $(JOB_OUTDIR) ]; then
	  mkdir -p $(JOB_OUTDIR) && \
	  cp $$ftojdir $(JOB_OUTDIR) && \
	  $(sinfo) "$$ftojdir copied to $(JOB_OUTDIR)" || errs=$$((errs+1))
	fi
	[ $$errs -eq 0 ]


# an anonymized doc is created from a standard one, there's no explicit
# source file, though a symlink is expected.
# preptex acts like '$(latex_exe) "\def\clOptAnonym{}\input{$(output).tex}"'
.PHONY: _anonym _prepare_anon txt
_anonym: output := $(anonym_tag)-$(input)
_anonym:
	$(sinfo) "output: $(output)"
	$(MAKE) input=$(input) output=$(anonym_tag)-$(input) _prepare_anon || exit 1
	$(latex_exe) "`$(prptex_exe) $(texdefs) -D Anonym $(output).tex`" || exit 1
	mv $(srcdir)/$(custom_out_pdf) $(outdir)
	$(MAKE) input=$(outdir)/$(output) txt
	$(pdfrdr_exe) $(outdir)/$(output).pdf &

# @$(MAKE) source=$(outdir)/$(output).pdf _tojobdir

# @mv $(srcdir)/$(output).pdf $(outdir)

_checks:
	@errs=0
	[ "$(custom_name)" ] || {
	  errs=$$((errs+1))
	  $(serror) "custom_name: not defined"
	}
	[ $$errs -eq 0 ]

# base name as input, i.e. file name without '.<lang>.tex' extension
#  $ make custom_name=<cv-basename> custom
# Workflow:
# - compile
# - mv result to $(outdir)
# - txt conversion
# - make symlinks with author's name
# - copy to $(job_outdir)
# All input paths are without extension, as that's added by the make subtargets
.PHONY: custom _checks _blatex txt _symlinks _runreader _anonym
custom: inputfp := $(custom_dname)/$(custom_bname)
custom: symlnk := $(subst .,_,$(subst .$(PLANG),,$(outf_prfx)--$(doc_type)--$(custom_bname)))
custom:	_checks $(custom_src_tex)
	@$(MAKE) input=$(custom_dname)/$(custom_bname) _blatex && \
	  mv $(srcdir)/$(custom_out_pdf) $(outdir) || exit 1
	$(MAKE) input=$(outdir)/$(custom_bname) txt || exit 1
	$(MAKE) target=$(custom_bname) symlnk=$(symlnk) _symlinks || exit 1

	if ! [ "$(NO_READER)" ]; then
	  odir=$(JOB_OUTDIR)
	  odir=$${odir:-$(outdir)}
	  $(MAKE) pdffile="$$odir/$(symlnk).pdf" _runreader || exit 1
	fi

	if [ "$(ANONYMOUS)" ]; then
	  $(MAKE) input=$(custom_bname) _anonym || exit 1;
	fi

	$(sinfo) "Compilation OK! See output in '$(outdir)/'"


.PHONY: proper clean help
clean:
	@find $(srcdir) -regextype posix-egrep \
	  -regex '.*?/_region_.*|.*?\.(log|aux|bbl|blg|out|log|dvi|toc|pdf)$$' -print0 | xargs -0 rm -f

proper:	clean
	@find $(outdir) -regextype posix-egrep -regex '.*?\.(pdf|txt)$$' -print0 | xargs -0 rm -f
	find $(srcdir) -regextype posix-egrep -regex '.*?~$$' -print0 | xargs -0 rm -f


define _help_msg :=
Usage:

  make [OPTIONS] custom_name=<texfile-basename> [custom]

where <texfile> is the input file name without the <PLANG>.tex extension.

Options:

  ANONYMOUS=<whatever>  ()   -- build anonymized documents too
  NO_READER=<whatever>  ()   -- do not lunch a PDF reader
  BIBTEX=<whatever>     ()   -- run bibtex
  PLANG=<lang-tag>      (en) -- language (fr|en|...)
  JOB_OUTDIR=<dir>      ()   -- where to place copies of produced documents
  DEF_MYJOB=<string>    ()   -- your job description in single quotes
  DEF_MYSELF=<string>   ()   -- your name in single quotes

Examples:

  make DEF_MYSELF='Mr So & So' DEF_MYJOB='what I do' \
    JOB_OUTDIR=/place/ouput/here/ \
    custom_name=CV-Technical_manager ANONYMOUS=1 custom

endef

# the shell no-op silences 'nothing to be done...' messages
help:
	@: $(info $(_help_msg))
