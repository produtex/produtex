%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% contract.cls
% $Id$
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Copyright 2012 Marco Emilio Poleggi
%
%% This file is part of produtex.
%
%% produtex is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%
%% produtex is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%
%% You should have received a copy of the GNU General Public License
%% along with produtex.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{contract}[2012/12/04 v0.2 a class for business contracts]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage{kvoptions}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\DeclareDefaultOption{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessKeyvalOptions*
\LoadClass[11pt,a4paper]{article}
% \RequirePackage[mydeffile=sources/mydefs.tex]{produ}
\RequirePackage{produ}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% class options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% none for now

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% globals -- filled in with real data by macros or explicitly
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% none


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% class macros
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% template definitions set some formulas, such as the title (which can be
% overridden by \title)
\newcommand{\templateNDA}{\title{\formulasDB@nda**}}

\newcommandx*{\subject}[1][1=]{%
  \gdef\produ@contract@subject{#1}
}
% Will print \dotfill if \project is void
\newcommand*{\insertproject}{%
  \produ@mboxDotfillCsvoid{\produ@contract@subject}
}


\newcommandx{\offerer}[3][
  % empty defaults
  1=,% name
  2=,% address lines
  3= % company registration #
]{%
  \gdef\produ@contract@offererName{#1}
  \gdef\produ@contract@offererAddress{#2}
  \gdef\produ@contract@offererRegNum{#3}
}

% to avoid confusion we use 'partner' instead of 'offeree'
\newcommandx{\partner}[3][
  % empty defaults
  1=,% name
  2=,% address lines
  3= % company registration #
]{%
  \gdef\produ@contract@partnerName{#1}
  \gdef\produ@contract@partnerAddress{#2}
  \gdef\produ@contract@partnerRegNum{#3}
}

\renewcommand{\maketitle}{%
  \bigskip
  \begin{center}
    \begin{minipage}[c]{\linewidth}
      \centering
      \Huge\textbf{\textsf{\@title}}\par
      \bigskip
      \large\formulasDB@between\par
    \end{minipage}
    % top: offerer
    \begin{minipage}[c]{.5\linewidth}
      \centering
      \vspace{20pt}
      \large
      \textbf{\produ@dotfillCsvoid{\produ@contract@offererName}}\\
      \produ@dotfillCsvoid{\produ@contract@offererAddress}\\
      \produ@dotfillCsvoid{\produ@contract@offererRegNum}\\
      (\formulasDB@hereafterRef~\emph{\formulasDB@offerer*})\\
      \bigskip
      \formulasDB@and~\\
      \bigskip
      % bottom: partner
      \textbf{\produ@dotfillCsvoid{\produ@contract@partnerName}}\\
      \produ@dotfillCsvoid{\produ@contract@partnerAddress}\\
      \produ@dotfillCsvoid{\produ@contract@partnerRegNum}\\
      (\formulasDB@hereafterRef~\emph{\formulasDB@partner*})
    \end{minipage}
  \end{center}
  \bigskip
  \large\textsf{\textbf{\formulasDB@subject*:} \produ@dotfillCsvoid{\produ@contract@subject}}~(\formulasDB@hereafterRef~\emph{\formulasDB@project*})
  \bigskip
  \null\vfil
}


\newcommandx{\closing}[7][%
  1=\formulasDB@madeIn*,
  2=\dotfill, % where
  3=\dotfill, % when
  4=\dotfill, % offerer's company
  5=\dotfill, % offerer's name
  6=\dotfill, % partner's company
  7=\dotfill, % partner's name
  usedefault
]{%
  \vspace{1cm}
  \noindent
  \begin{minipage}[t]{0.6\linewidth}
    #1~#2,~\formulasDB@the~#3\\
  \end{minipage}
  \null\vfil
  % left: offerer
  \begin{center}
    \begin{minipage}[t]{0.48\linewidth}
      \formulasDB@onBehalf*:\\
      \squote{#4}
      \formulasDB@theOfferer*\\
      \squote{#5}
    \end{minipage}
    \hspace{0.02\linewidth}
    % right: partner
    \begin{minipage}[t]{0.48\linewidth}
      \formulasDB@onBehalf*:\\
      \squote{#6}
      \formulasDB@thePartner*\\
      \squote{#7}
    \end{minipage}
  \end{center}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% base layout
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\layoutwide
% \layoutcompact
% \layoutcv
