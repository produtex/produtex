################################################################################
# Makefile
# $Id$
################################################################################
# Copyright 2011-2018 Marco Emilio Poleggi
#
# This file is part of produtex.
#
# produtex is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# produtex is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with produtex.  If not, see <http://www.gnu.org/licenses/>.
################################################################################
.ONESHELL:

# character variables for easier substitutions.
,			:= ,
space		:= $(subst ,, ) # nasty trick <https://stackoverflow.com/questions/10571658/gnu-make-convert-spaces-to-colons>
# space 		+=
# $(space) 	:=
# $(space) 	+=

# basic variables
distname	:= produtex
version		:= $(shell cat VERSION)
dist_archive	:= $(distname)-v$(version).tar.gz
dist_files	:= $(shell cat MANIFEST)
doc_srcdir	:= doc
doc_exmpldir	:= $(doc_srcdir)/examples
tex_srcdir	:= latex
tex_srcbmrdir	:= $(tex_srcdir)/beamer
tex_srcpdudir	:= $(tex_srcdir)/produ
bin_srcdir	:= scripts
exmpl_sbox	:= $(HOME)/tmp/$(distname)/example
# make sure these defs match those in produtex.make!
exmpl_outdir	:= $(exmpl_sbox)/output
exmpl_srcdir	:= $(exmpl_sbox)/sources
exmpl_sctdir	:= $(exmpl_srcdir)/sections

# sanity checks on executables
kpsewhich	:= $(or $(shell which kpsewhich 2>&-),$(error kpsewhich not found: make sure you have a full tetex installation))
pdftotext	:= $(or $(shell which pdftotext 2>&-),$(warning pdftotext not found: PDF to TXT conversion will *not* work -- suggestion: install poppler))
perl		:= $(or $(shell which perl 2>&-),$(warning perl not found: TXT adjustments will *not* work))
tree		:= $(shell which tree 2>&-)

# Make options
# For kpsewhich, see your texmf conf file, usually '/some/where/texmf.cnf'.
PREFIX		:=
TEXFM_DDIR	:= $(or $(shell $(kpsewhich) --var-value=TEXMFLOCAL),/usr/local/share/texmf)
SCRIPT_DDIR	:= /usr/local/bin

# paths
scripts		:= $(addprefix $(bin_srcdir)/,produtex.make cfilter preptex)
tex_mods	:= $(addprefix $(tex_srcdir)/,produ.sty)
tex_bmrs	:= $(addprefix $(tex_srcbmrdir)/,beamerthememep.sty)
tex_docs	:=
tex_exmplmstr	:= $(addprefix $(doc_exmpldir)/,$(addsuffix .tex,example-CV example-mydefs))
tex_exmplssec	:= $(addprefix $(doc_exmpldir)/,$(addsuffix .tex,example-sect_Experience))
tex_exmpls	:= $(tex_exmplmstr) $(tex_exmplssec)
bin_instdir	:= $(PREFIX)/$(SCRIPT_DDIR)
tex_instroot	:= $(PREFIX)/$(TEXFM_DDIR)
tex_instdir	:= $(tex_instroot)/tex/latex/$(distname)
tex_instbmrdir	:= $(tex_instdir)/beamer
tex_doc_instdir	:= $(tex_instroot)/doc/latex/$(distname)
tex_exmpl_instdir	:= $(tex_doc_instdir)/examples

################################################################################
# auxiliary functions
################################################################################
_minfo		= $(info [INFO] $(1))
_mwarn		= $(warning [WARN] $(1))
_merro		= $(error [ERROR] $(1))
_echoe		= echo -e
sinfo		= $(_echoe) '[INFO] '
swarn		= $(_echoe) '[WARN] '
serror		= $(_echoe) '[ERROR] '

# find installation paths beginning with '$(1)' in the MANIFEST -- the
# blank at end sequence ') ))' is really needed, DO NOT remove it!
_find_mpaths	= $(strip $(subst $(space), ,$(shell sed -nr 's%(^$(1)/([^/].+/)*?)[^/]+$$%\1% p' MANIFEST) ))

# find installation files whose path begins with '$(1)' in the MANIFEST
# _find_mfiles	= $(subst $(,), ,$(shell sed -nr 's%^$(1)/\{?([^/{}]+)\}?$$%\1% p' MANIFEST))
# # rebuild the full path to a MANIFEST's file given its path
# _mfiles_for	= $(foreach path,$(call _find_mpaths,$(1)),$(addprefix $(path)/,$(call _find_mfiles,$(path))))

# shell macro to find MANIFEST files -- to be called inside a target
# (_find_mfiles wouldn't work...): '$path' acts as a hardcoded parameter.  sed
# output is filtered via 'ls' to expand any pattern (Make's 'wildcard' is too
# limited)
_sh_mfiles	:= $$(shopt -s extglob; eval ls `sed -nr "\%^$${path%%+(/)}/+[^/]+$$% p" MANIFEST`)


################################################################################
# targets
################################################################################
# precalculates paths...
doc_mpaths	:= $(call _find_mpaths,$(doc_srcdir))
tex_mpaths	:= $(call _find_mpaths,$(tex_srcdir))
# tex_mfiles	:= $(call mfiles_for,$(tex_srcdir))

.PHONY: all help
all: help

_uninstall_doc:
	rm -rf $(tex_doc_instdir)

_uninstall_tex:
	rm -rf $(addprefix $(tex_instdir)/,$(tex_mpaths))

# use with utmost care! Scripts and docs are not removed...
uninstall:	_uninstall_doc _uninstall_tex

# target files are there just to trigger; real names are taken from the MANIFEST
_install_bin:	$(addsuffix /*,$(bin_srcdir))
	instdir=$(bin_instdir)
	install -d $$instdir
	path=$(bin_srcdir)
	files=$(_sh_mfiles)
	install -m 0544 -t $$instdir $$files
	$(sinfo) "Bin files installed"

_install_doc:	$(addsuffix *,$(doc_mpaths))
	install -d $(tex_doc_instdir)
	for path in $(doc_mpaths); do
		instdir=$(tex_doc_instdir)
		install -d $$instdir
		files=$(_sh_mfiles)
		install -m 0444 -t $$instdir $$files
	done
	$(sinfo) "Examples have been installed in $(tex_doc_instdir)"

_install_tex:	$(addsuffix /*,$(tex_mpaths))
	install -d $(tex_instdir)
	for path in $(tex_mpaths); do
		instdir=$(tex_instdir)/$$path
		install -d $$instdir
		files=$(_sh_mfiles)
		install -m 0444 -t $$instdir $$files
	done
	$(sinfo) "Latex files installed"

install:	_install_bin  _install_doc _install_tex
	$(sinfo) 'Installation OK'
	if ! [[ `$(kpsewhich) --var-value=TEXINPUTS` =~ $(tex_instroot) ]]; then
		$(swarn) "$(tex_instroot) is not in your TEXINPUTS"
	fi

dist:
	tar zcf $(dist_archive) $(dist_files)
	$(sinfo) "Distribution archive is $(dist_archive)"

clean:
	$(sinfo) 'Cleaning up...'
	rm -f $(dist_archive)

proper:		clean
	$(sinfo) 'Propering up...'
	find ./ -regextype posix-egrep
		-regex '.*?(~|bak)$$' -print0 | xargs -0 rm -f

# doc:		$(tex_docs) $(tex_exmpls)
# 	$(serror) 'doc: target not implemented yet'

# build the bundled example in a sandbox
_make_exmpl_cmd	:= $(MAKE) custom_name=example-CV PLANG=fr ANONYMOUS=1 custom clean
example:	install
	$(sinfo) 'Creating examples sandbox in "$(exmpl_sbox)"...'
	rm -rf $(exmpl_sbox)
	mkdir -p $(exmpl_sctdir) $(exmpl_outdir)
	cp $(tex_exmplmstr) $(exmpl_srcdir)
	cp $(tex_exmplssec) $(exmpl_sctdir)
	cd $(exmpl_sbox) &&
		ln -sf $(exmpl_srcdir)/example-CV.tex $(exmpl_srcdir)/example-CV.fr.tex &&
		ln -sf $(bin_instdir)/produtex.make Makefile &&
		$(_make_exmpl_cmd) &&
		echo -e "example built by command:\n  $$ $(_make_exmpl_cmd)" > $(exmpl_sbox)/README
	if [ "$(tree)" ]; then
		$(sinfo) 'Sandbox has the following structure:'
		$(tree) -n --charset=iso-8859-15 $(exmpl_sbox)
	else
		$(sinfo) 'Sandbox created. Have a look at $(exmpl_sbox)'
	fi

define _help_msg :=
Usage:

  make [OPTIONS] TARGET [TARGET...]

where TARGET can be

  clean
  dist
  #doc					BROKEN!
  example
  help
  install
  proper

Options

  PREFIX=<path>         ()      -- installation prefix
  TEXFM_DDIR=<path>     (TEXMFLOCAL|/usr/local/share/texmf)
                                -- installation path to your local TeXFM distro
  SCRIPT_DDIR=<path>    (/usr/local/bin)
                                -- installation path for local binaries
  exmpl_sbox=<path>     ($(HOME)/tmp/$(distname)/example)
                                -- path to the example sandbox
Examples:

  $$ make PREFIX=/my/inst/path install
endef

# the shell no-op silences 'nothing to be done...' messages
help:
	@: $(info $(_help_msg))
